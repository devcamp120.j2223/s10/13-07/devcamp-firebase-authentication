import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyB27ENArGDdRrqFK2eE9e7eO-t19rnSioc",
    authDomain: "devcamp-r23.firebaseapp.com",
    projectId: "devcamp-r23",
    storageBucket: "devcamp-r23.appspot.com",
    messagingSenderId: "502061800692",
    appId: "1:502061800692:web:61283c013a4d0d420692a1"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();